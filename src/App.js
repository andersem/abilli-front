import React, {Component} from 'react';
import './App.css';
import {Button} from 'rmwc/Button';
import CodeMirror from 'react-codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import {
    Card,
    CardAction,
    CardActions,
    CardActionButtons,
} from 'rmwc/Card';

import { Snackbar } from 'rmwc/Snackbar';


class App extends Component {
    cmOptions = {
        lineNumbers: true,
        mode: 'javascript'
    };

    render() {
        return (
            <div className="App">
                <Card>
                    <CodeMirror value={this.props.editingGeoJSON} onChange={this.props.onUpdateGeoJSON} options={this.cmOptions}/>
                    <p className="errormessage">{this.props.editingGeoJSONError}</p>
                    <CardActions>
                        <CardActionButtons>
                            <CardAction onClick={this.props.onSetGeoJSON}>Show GeoJSON</CardAction>
                        </CardActionButtons>
                    </CardActions>
                </Card>
                <div>
                </div>
                <div>
                    <Button onClick={this.props.onUnion} disabled={this.props.features.length !== 2}>Union</Button>
                    <Button onClick={this.props.onIntersect}
                            disabled={this.props.features.length !== 2}>Intersect</Button>
                    <Button onClick={this.props.onSave} disabled={this.props.features.length !== 1}>Save</Button>
                </div>
                <div>
                    {this.props.serverFeaturesError && <p>{this.props.serverFeaturesError}</p>}
                    <ul>
                        {this.props.serverFeatures.map(sf => {
                            return <li key={sf.id}>{sf.id.substring(0, 5)} - {sf.created} <Button onClick={() => this.props.addFeature(sf)}>Add</Button></li>;
                        })}

                    </ul>
                </div>
                <Snackbar
                    show={!!this.props.editingGeoJSONError}
                    message={this.props.editingGeoJSONError}
                />
            </div>
        );
    }
}

export default App;
