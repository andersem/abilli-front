function getRandomInt() {
    const byteArray = new Uint8Array(1);
    window.crypto.getRandomValues(byteArray);
    return byteArray[0];
}

export default getRandomInt;