import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'leaflet/dist/leaflet.css';
import Map from './map';
import geoj from './geoj';
import 'material-components-web/dist/material-components-web.min.css';
import PolyBool from 'polybooljs';
import GJV from "geojson-validation";


const state = {
    layers: [],
    serverFeatures: [],
    serverFeaturesError: '',
    editingGeoJSON: JSON.stringify(geoj, null, 2),
    editingGeoJSONError: '',
    editingGeoJSONWarning: ''
};

async function fetchPolygons () {
    try {
        const response = await fetch('https://f5kr2ylkli.execute-api.eu-central-1.amazonaws.com/Prod');
        const json = await response.json();
        state.serverFeatures = json.features.sort((a, b) => {
            return (a.created > b.created) ? -1 : (a.created < b.created) ? 1 : 0;
        }).map(item => item);
        state.serverFeaturesError = '';
        render();
    } catch(err) {
        console.log("ERROR", err);
        state.serverFeaturesError = 'Couldn\'t fetch polygons from server';
        render();
    }
}

fetchPolygons();

function performOperation(map, polygon1, polygon2, func) {
    const union = func(polygon1, polygon2);
    const unionAsGeoJSON = PolyBool.polygonToGeoJSON(union);
    const currentGeoJson = map.geoJson;
    const withoutSelected = currentGeoJson.features.filter(feature => !state.layers.map(layer => layer.feature.id).includes(feature.id));

    withoutSelected.push({
            type: "Feature",
            properties: {},
            geometry: unionAsGeoJSON
        }
    );

    currentGeoJson.features = withoutSelected;
    map.setGeoJson(currentGeoJson, onSelectLayer);
    state.layers = [];
    render();
}

const onSave = async () => {
    if (state.layers.length === 1) {
        try {
            const result = await fetch('https://f5kr2ylkli.execute-api.eu-central-1.amazonaws.com/Prod', {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(state.layers[0].feature), // data can be `string` or {object}!
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const json = await result.json();
            console.log('Success:', json);
            fetchPolygons();
        } catch (err) {
            console.error('Error:', err);
        }
    }
};

const onUnion = () => {
    performOperation(
        abilliMap,
        PolyBool.polygonFromGeoJSON(state.layers[0].feature.geometry),
        PolyBool.polygonFromGeoJSON(state.layers[1].feature.geometry),
        PolyBool.union);
};

const onIntersect = () => {
    performOperation(
        abilliMap,
        PolyBool.polygonFromGeoJSON(state.layers[0].feature.geometry),
        PolyBool.polygonFromGeoJSON(state.layers[1].feature.geometry),
        PolyBool.intersect);
};

const onSelectLayer = (smth, cb) => {
    const found = state.layers.find(layer => layer.feature.id === smth.feature.id);
    if (found) {
        state.layers = state.layers.filter(layer => layer.feature.id !== smth.feature.id);
    } else {
        state.layers.unshift(smth);
    }

    let removed;
    if (state.layers.length > 2) {
        removed = state.layers.pop();
    }

    cb(state.layers, removed);
    render();
};

const onSetGeoJSON = () => {
    const geojson = JSON.parse(state.editingGeoJSON);
    if(GJV.valid(geojson)){
        abilliMap.setGeoJson(geojson, onSelectLayer);
        state.editingGeoJSONError = '';
    } else {
        state.editingGeoJSONError = 'Invalid GeoJSON';
    }
    render();
};

const onUpdateGeoJSON = (geojson) => {
    state.editingGeoJSON = geojson;
    render();
};

const addFeature = (serverFeature) => {
    serverFeature.feature.id = null;
    const currentGeoJson = abilliMap.geoJson;
    currentGeoJson.features.push(serverFeature.feature);
    abilliMap.setGeoJson(currentGeoJson, onSelectLayer);
    render();
};

const abilliMap = new Map('abillimap');

const render = () => {
    ReactDOM.render(<App
        onUnion={onUnion}
        onIntersect={onIntersect}
        onSave={onSave}
        features={state.layers.map(layer => layer.feature)}
        serverFeatures={state.serverFeatures}
        serverFeaturesError={state.serverFeaturesError}
        editingGeoJSON={state.editingGeoJSON}
        onUpdateGeoJSON={onUpdateGeoJSON}
        onSetGeoJSON={onSetGeoJSON}
        editingGeoJSONError={state.editingGeoJSONError}
        addFeature={addFeature}
    />, document.getElementById('root'));
};

render();
