import L from "leaflet";
import getRandomInt from './utils/randomInt';

const hoverStyle = {
    weight: 3,
    color: '#3B555C',
    dashArray: '',
    fillOpacity: 0.6
};

const selectStyle = {
    weight: 3,
    color: 'red',
    dashArray: '',
    fillOpacity: 0.6
};

class Map {

    constructor(mapDivId) {
        this.selectedLayers = [];
        this.mapDivId = mapDivId;
        this.map = L.map(mapDivId).setView([38.82259, -2.8125], 3);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
    }

    highlightOnHover = (evt) => {
        if (!this.selectedLayers.map(l => l.feature.id).includes(evt.target.feature.id)) {
            const feature = evt.target;
            feature.setStyle(hoverStyle);
            if (!L.Browser.ie && !L.Browser.opera) {
                feature.bringToFront();
            }
        }
    };

    highlightOnSelect = (feature) => {
        feature.setStyle(selectStyle);
        if (!L.Browser.ie && !L.Browser.opera) {
            feature.bringToFront();
        }
    };

    fitBounds(bounds) {
        this.map.fitBounds(bounds);
    }

    resetHighlight = (evt) => {
        if (!this.selectedLayers.map(s => s.feature.id).includes(evt.target.feature.id)) {
            this.geoLayer.resetStyle(evt.target);
        }
    };

    setGeoJson = (geoj, onSelectLayer) => {
        this.geoJson = geoj;

        if (this.geoLayer) {
            this.geoLayer.removeFrom(this.map);
        }

        this.geoLayer = L.geoJSON(geoj, {
            onEachFeature: (feature, layer) => {
                if (!feature.id) {
                    feature.id = getRandomInt();
                }
                layer.on({
                    mouseover: this.highlightOnHover,
                    mouseout: this.resetHighlight,
                    click: (evt) => {
                        onSelectLayer(evt.target, ((selected, removed) => {
                            this.selectedLayers = selected;

                            if (selected.map(s => s.feature.id).includes(evt.target.feature.id)) {
                                this.highlightOnSelect(evt.target);
                            } else {
                                this.geoLayer.resetStyle(evt.target);
                            }

                            if (removed) {
                                this.geoLayer.resetStyle(removed);
                            }
                        }));
                    }
                })
            },
            style: function (feature) {
                return {color: feature.properties.color};
            }
        }).addTo(this.map);

        this.fitBounds(this.geoLayer.getBounds());
    };

}

export default Map;

